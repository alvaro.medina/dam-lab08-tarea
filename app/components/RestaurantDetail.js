import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';


export default class myList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            textValue: 0,
            count: 0,
            items: [],
            error: null,
            id: this.props.route.params.itemId,
            imagen: this.props.route.params.itemImage,
            titulo: this.props.route.params.itemTitle,
            informacion: this.props.route.params.itemInfo,
            latitude: this.props.route.params.itemLat,
            longitude: this.props.route.params.itemLong,
            estado: this.props.route.params.itemEst
        };
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text></Text>
                    <View>
                        <Image source={{ uri: this.state.imagen }} style={styles.itemImage} resizeMode="cover" />
                    </View>
                    <View>
                        <Text style={styles.txt1}>Descripción:</Text>
                        <Text style={styles.description}>{this.state.informacion}</Text>
                        <Text style={styles.description}>{this.state.latitude}</Text>
                        <Text style={styles.description}>{this.state.longitude}</Text>
                    </View>
                    <View>
                        <TouchableOpacity style={styles.button}
                            onPress={() => this.props.navigation.navigate('Maps', {
                                id: this.props.route.params.itemId,
                                titulo: this.props.route.params.itemTitle,
                                latitude: this.props.route.params.itemLat,
                                longitude: this.props.route.params.itemLong,
                                estado: this.props.route.params.itemEst
                            })}>
                            <Text style={styles.textButton}>UBICACIÓN</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text></Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#BBDEFB',
    },
    itemContainer: {
        backgroundColor: '#BBDEFB',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    description: {
        fontSize: 17,
        paddingLeft: 20,
        textAlign: 'justify',
        paddingRight: 20,
    },
    itemImage: {
        height: 400,
        width: 250,
        alignSelf: 'center',
    },
    txt1: {
        fontSize: 35,
        paddingLeft: 10,
        fontFamily: 'cursive',

    },
    button: {
        paddingLeft: 30,
        paddingRight: 30,
        padding: 5,
        marginTop: 20,
        alignSelf: 'center',
        backgroundColor: '#000000',
        borderRadius: 5,
    },
    textButton: {
        color: 'white',
        fontSize: 20,
    },
});