import React, { Component } from 'react';
import { View, Text } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import RestaurantList from './app/components/RestaurantList';
import RestaurantDetail from './app/components/RestaurantDetail';
import RestaurantMaps from './app/components/RestaunrantMaps';

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={RestaurantList}
            options={{
              title: 'Lista de Restaurantes',
              headerStyle: {
                backgroundColor: '#7E57C2',
              },
              headerTitleStyle: {
                fontFamily: 'cursive',
                color: '#fff',
                fontSize: 45,
              },
            }}
          />
          <Stack.Screen
            name="Details"
            component={RestaurantDetail}
            options={{
              title: 'Detalles del Restaurante',
              headerStyle: {
                backgroundColor: '#7E57C2',
              },
              headerTitleStyle: {
                fontFamily: 'cursive',
                color: '#fff',
                fontSize: 40,
                backgroundColor: '#7E57C2',
              },
            }}
          />
          <Stack.Screen
            name="Maps"
            component={RestaurantMaps}
          />

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}